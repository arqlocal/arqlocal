if [ -d ./onejar/target ]; then
    echo "Arquivos encontrados" && exit 0
else
    curl -k https://nexus.cloudapps.indraweb.net/repository/settings_bpo/maven/m4b.xml --output settings.xml
    mvn clean install package -DskipTests=true -s settings.xml -Dmaven.wagon.http.ssl.insecure=true -Dmaven.wagon.http.ssl.allowall=true -Dmaven.wagon.http.ssl.ignore.validity.dates=true
fi

