FROM openjdk:16-ea-23-jdk-alpine3.12

ADD /onejar* /pwm/onejar

WORKDIR pwm/onejar/target

CMD java -jar pwm-onejar-2.1.0-SNAPSHOT.jar -applicationPath /pwm/onejar/target/ 

EXPOSE 8443

